//
//  StaticPro.m
//  SceneEditor
//
//  Created by lh on 14-3-20.
//  Copyright (c) 2014年 lh. All rights reserved.
//  www.9miao.com

#import "StaticPro.h"
#import "ASIHTTPRequest.h"
#import "SBJson.h"
@implementation StaticPro
@synthesize base_attack;
@synthesize base_hp;
@synthesize base_strike;
@synthesize damage_rate;
@synthesize defense_cf;
@synthesize deflect_cf;
@synthesize dodge_cf;
@synthesize hitrate_cf;
@synthesize parry_cf;
@synthesize parry_rate;
@synthesize strike_cf;
@synthesize strike_rate;
@synthesize strike_result_cf;
@synthesize tenacity_cf;
@synthesize staticProperties;
@synthesize avoid_rate;

-(void)setStaticProperties{
    staticProperties=[[NSMutableDictionary alloc] init];
    [staticProperties setValue:base_hp forKey:@"hp"];
    [staticProperties setValue:base_attack forKey:@"att"];
    [staticProperties setValue:defense_cf forKey:@"def"];
    [staticProperties setValue:dodge_cf forKey:@"agl"];
    

    [staticProperties setValue:tenacity_cf forKey:@"tou"];
    [staticProperties setValue:parry_cf forKey:@"par"];
    [staticProperties setValue:deflect_cf forKey:@"ske"];
    [staticProperties setValue:strike_cf forKey:@"cri"];
    [staticProperties setValue:strike_result_cf forKey:@"crp"];
    [staticProperties setValue:hitrate_cf forKey:@"dex"];
//    [staticProperties setValue:damage_rate forKey:@"damage_rate"];
//    [staticProperties setValue:avoid_rate forKey:@"avoid_rate"];
//    [staticProperties setValue:parry_rate forKey:@"parry_rate"];
//    [staticProperties setValue:strike_rate forKey:@"strike_rate"];
    
    SBJsonWriter *writer=[[SBJsonWriter alloc] init];
    
    NSString *jsonStr=[writer stringWithObject:staticProperties];
    
  
    
   NSString *str1= [jsonStr stringByReplacingOccurrencesOfString:@"{" withString:@""];
   NSString *str2=  [str1 stringByReplacingOccurrencesOfString:@"}" withString:@""];
    NSString *str3= [str2 stringByReplacingOccurrencesOfString:@":" withString:@"="];
    NSString *str4=[str3 stringByReplacingOccurrencesOfString:@"\"" withString:@""];

    NSLog(@"%@",str4);
    NSString *str=[[NSString alloc] initWithFormat:@"http://127.0.0.1:11008/updastemodulus?json=%@",str4];//172.16.18.25

    NSLog(@"%@",str);
    
    NSURL *url = [NSURL URLWithString:[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
	[request setTimeOutSeconds:5];
	[request setDelegate:self];
	[request startAsynchronous];
    
}
@end
