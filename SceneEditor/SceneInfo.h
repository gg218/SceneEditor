//
//  SceneInfo.h
//  SceneEditor
//
//  Created by lh on 14-1-9.
//  Copyright (c) 2014年 lh. All rights reserved.
//  www.9miao.com

#import <Foundation/Foundation.h>
#import "Person.h"

@interface SceneInfo : NSObject

@property (retain,nonatomic) NSMutableArray *enemyArr;
@property (retain,nonatomic) NSString *resourceName;
@property (retain,nonatomic) NSString *right;
@property (retain,nonatomic) NSString *left;
@property (retain,nonatomic) NSString *down;
@property (retain,nonatomic) NSString *up;
@property (retain,nonatomic) NSString *gold;
@property (retain,nonatomic) NSString *exp;
@property (retain,nonatomic) NSString *sceneId;
@property (retain,nonatomic) NSString *sceneName;
@property (retain,nonatomic) NSString *nextSceneId;
@property (retain,nonatomic) NSString *monsterId;
//
//@property (retain,nonatomic) NSString *releaseRole;
@property (retain,nonatomic) NSString *sceneType;
@end
