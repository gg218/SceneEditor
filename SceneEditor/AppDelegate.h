//
//  AppDelegate.h
//  SceneEditor
//
//  Created by lh on 14-1-9.
//  Copyright (c) 2014年 lh. All rights reserved.
//  www.9miao.com

#import <Cocoa/Cocoa.h>
#import "SceneInfo.h"
#import "SBJson.h"
#import "NSObject+SBJson.h"
#import "SBJsonWriter.h"
#import "SBJsonParser.h"
#import "StaticPro.h"
@interface AppDelegate : NSObject <NSApplicationDelegate,NSTableViewDataSource,NSTableViewDelegate,NSComboBoxDataSource,NSComboBoxDelegate,SBJsonStreamWriterDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSTextField *resName;
@property (assign) IBOutlet NSTextField *up;
@property (assign) IBOutlet NSTextField *down;
@property (assign) IBOutlet NSTextField *left;
@property (assign) IBOutlet NSTextField *right;
@property (assign) IBOutlet NSTextField *gold;
@property (assign) IBOutlet NSTextField *exp;
@property (assign) IBOutlet NSTextField *sceneId;
@property (assign) IBOutlet NSTextField *nextsceneId;
@property (assign) IBOutlet NSTextField *sceneName;
@property (assign) IBOutlet NSTextField *monsterName;
@property (assign) IBOutlet NSTableView *tableView;
@property (retain,nonatomic)NSMutableArray *person;
@property (assign) IBOutlet NSTabView *tabView;
//@property (assign) IBOutlet NSComboBox *releaseRoles;

@property (assign) IBOutlet NSComboBox *sceneType;

//@property (assign) IBOutlet NSTextField *eid;


@property (retain,nonatomic) SceneInfo *sceneInfo;


@property (assign) IBOutlet NSButton *saveBtn;


//@property (assign)IBOutlet NSComboBox *selectPet;
@property (assign) IBOutlet NSButton *sureBtn;

@property (retain,nonatomic) Person *m_person;
@property (retain,nonatomic)StaticPro *m_static;
-(IBAction)savePetInfo:(id)sender;
-(IBAction)addSelectTableEnemy:(id)sender;
-(IBAction)removeSelectTableEnemy:(id)sender;
-(IBAction)selectBtn:(id)sender;
-(IBAction)changeTab:(id)sender;
-(IBAction)setcomplete:(id)sender;
-(IBAction)saveFile:(id)sender;
-(IBAction)updateStaticValue:(id)sender;
-(void)setPersonValue:(Person *)sender;
-(IBAction)getModulus:(id)sender;
//@property (assign) IBOutlet NSTextField *level;//等级
@property (assign) IBOutlet NSTextField *hp;
@property (assign) IBOutlet NSTextField *typeId;//模板id
@property (assign) IBOutlet NSTextField *attack;//攻击
@property (assign) IBOutlet NSTextField *defence;//防御
@property (assign) IBOutlet NSTextField *hit;//命中
@property (assign) IBOutlet NSTextField *tenacity;//韧性
@property (assign) IBOutlet NSTextField *dodge;//闪避
@property (assign) IBOutlet NSTextField *parry;//格挡
@property (assign) IBOutlet NSTextField *Crit;//暴击
@property (assign) IBOutlet NSTextField *Critpower;//暴击威力
@property (assign) IBOutlet NSTextField *Attackrange;//攻击范围
@property (assign) IBOutlet NSTextField *Movementspeed;//移动速度
@property (assign) IBOutlet NSTextField *Attackspeed;//攻击速度
@property (assign) IBOutlet NSTextField *basehp;//标准生命
@property (assign) IBOutlet NSTextField *baseattack;//标准攻击
@property (assign) IBOutlet NSTextField *basecri;//标准暴击值
@property (assign) IBOutlet NSTextField *Defensecoefficient;//防御系数
@property (assign) IBOutlet NSTextField *Dodgecoefficient;//闪避系数
@property (assign) IBOutlet NSTextField *Toughnessproperties;//韧性属性
@property (assign) IBOutlet NSTextField *Blockcoefficient;//格挡系数
@property (assign) IBOutlet NSTextField *Coefficientofskewness;//偏斜系数
@property (assign) IBOutlet NSTextField *Critratecoefficient;//暴击率系数
@property (assign) IBOutlet NSTextField *Critpowercoefficient;//暴击威力系数
@property (assign) IBOutlet NSTextField *Hitcoefficient;//命中系数
@property (assign) IBOutlet NSTextField *damagerate;//伤害率
@property (assign) IBOutlet NSTextField *escaperate;//闪避率
@property (assign) IBOutlet NSTextField *Blockrate;//格挡率
@property (assign) IBOutlet NSTextField *Critrate;//暴击率
@property (assign) IBOutlet NSButton *makefileBtn;
@property (assign) IBOutlet NSTextField *countofenemy;
@property (assign) IBOutlet NSComboBox *enemycount;
@property (assign) IBOutlet NSComboBox *enemylist;
@property (assign) IBOutlet NSTextField *samePeople;
@property (assign) IBOutlet NSTextField *delaytime;
@property (assign) IBOutlet NSComboBox *skillId;
@property (assign) IBOutlet NSComboBox *coatt;
@property (assign) IBOutlet NSComboBox *btype;
@property (assign) IBOutlet NSComboBox *color;
@property (assign) IBOutlet NSTextField *dou;
@property (assign) IBOutlet NSTextField *ske;
@property (assign) BOOL flag;
@property (assign) NSInteger tagnum;
@property (retain,nonatomic) NSMutableArray *petarr;
-(IBAction)makefile:(id)sender;
-(IBAction)getPetList:(id)sender;
-(IBAction)selectMonster:(id)sender;
-(IBAction)writeFinished:(id)sender;
-(IBAction)selectCounts:(id)sender;
-(IBAction)sameMonior:(id)sender;
-(IBAction)delaytime:(id)sender;

@end
